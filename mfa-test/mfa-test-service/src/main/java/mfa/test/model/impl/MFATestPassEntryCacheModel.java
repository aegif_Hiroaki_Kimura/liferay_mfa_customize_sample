/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import mfa.test.model.MFATestPassEntry;

/**
 * The cache model class for representing MFATestPassEntry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MFATestPassEntryCacheModel
	implements CacheModel<MFATestPassEntry>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MFATestPassEntryCacheModel)) {
			return false;
		}

		MFATestPassEntryCacheModel mfaTestPassEntryCacheModel =
			(MFATestPassEntryCacheModel)object;

		if (mfaTestPassEntryId ==
				mfaTestPassEntryCacheModel.mfaTestPassEntryId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, mfaTestPassEntryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{mfaTestPassEntryId=");
		sb.append(mfaTestPassEntryId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", password=");
		sb.append(password);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MFATestPassEntry toEntityModel() {
		MFATestPassEntryImpl mfaTestPassEntryImpl = new MFATestPassEntryImpl();

		mfaTestPassEntryImpl.setMfaTestPassEntryId(mfaTestPassEntryId);
		mfaTestPassEntryImpl.setUserId(userId);

		if (password == null) {
			mfaTestPassEntryImpl.setPassword("");
		}
		else {
			mfaTestPassEntryImpl.setPassword(password);
		}

		mfaTestPassEntryImpl.resetOriginalValues();

		return mfaTestPassEntryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		mfaTestPassEntryId = objectInput.readLong();

		userId = objectInput.readLong();
		password = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(mfaTestPassEntryId);

		objectOutput.writeLong(userId);

		if (password == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(password);
		}
	}

	public long mfaTestPassEntryId;
	public long userId;
	public String password;

}
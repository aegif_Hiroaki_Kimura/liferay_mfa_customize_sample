/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;

import java.util.Date;

import mfa.test.exception.DuplicateMFATestPassEntryException;
import mfa.test.model.MFATestPassEntry;
import mfa.test.service.base.MFATestPassEntryLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the mfa test pass entry local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>mfa.test.service.MFATestPassEntryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MFATestPassEntryLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=mfa.test.model.MFATestPassEntry",
	service = AopService.class
)
public class MFATestPassEntryLocalServiceImpl
	extends MFATestPassEntryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>mfa.test.service.MFATestPassEntryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>mfa.test.service.MFATestPassEntryLocalServiceUtil</code>.
	 */
	
	@Override
	public MFATestPassEntry addTestPassEntry(long userId, String password) throws PortalException{
		MFATestPassEntry mfaTestPassEntry = mfaTestPassEntryPersistence.fetchByUserId(userId);
		
		if (mfaTestPassEntry != null) {
			throw new DuplicateMFATestPassEntryException("User ID " + userId);
		}
		
		mfaTestPassEntry = mfaTestPassEntryPersistence.create(counterLocalService.increment());
		
		User user = userLocalService.getUserById(userId);
		
		mfaTestPassEntry.setUserId(userId);
		mfaTestPassEntry.setPassword(password);
		
		return mfaTestPassEntryPersistence.update(mfaTestPassEntry);
	}
	
	@Override
	public MFATestPassEntry fetchMFATestPassEntryByUserId(long userId) {
		return mfaTestPassEntryPersistence.fetchByUserId(userId);
	}
	
	
}
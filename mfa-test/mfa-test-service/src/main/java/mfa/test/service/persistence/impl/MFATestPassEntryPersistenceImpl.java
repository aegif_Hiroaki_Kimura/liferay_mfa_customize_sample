/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import mfa.test.exception.NoSuchEntryException;
import mfa.test.model.MFATestPassEntry;
import mfa.test.model.impl.MFATestPassEntryImpl;
import mfa.test.model.impl.MFATestPassEntryModelImpl;
import mfa.test.service.persistence.MFATestPassEntryPersistence;
import mfa.test.service.persistence.impl.constants.MFATestPassPersistenceConstants;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the mfa test pass entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = MFATestPassEntryPersistence.class)
public class MFATestPassEntryPersistenceImpl
	extends BasePersistenceImpl<MFATestPassEntry>
	implements MFATestPassEntryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>MFATestPassEntryUtil</code> to access the mfa test pass entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		MFATestPassEntryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathFetchByUserId;
	private FinderPath _finderPathCountByUserId;

	/**
	 * Returns the mfa test pass entry where userId = &#63; or throws a <code>NoSuchEntryException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @return the matching mfa test pass entry
	 * @throws NoSuchEntryException if a matching mfa test pass entry could not be found
	 */
	@Override
	public MFATestPassEntry findByUserId(long userId)
		throws NoSuchEntryException {

		MFATestPassEntry mfaTestPassEntry = fetchByUserId(userId);

		if (mfaTestPassEntry == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("userId=");
			sb.append(userId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchEntryException(sb.toString());
		}

		return mfaTestPassEntry;
	}

	/**
	 * Returns the mfa test pass entry where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @return the matching mfa test pass entry, or <code>null</code> if a matching mfa test pass entry could not be found
	 */
	@Override
	public MFATestPassEntry fetchByUserId(long userId) {
		return fetchByUserId(userId, true);
	}

	/**
	 * Returns the mfa test pass entry where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching mfa test pass entry, or <code>null</code> if a matching mfa test pass entry could not be found
	 */
	@Override
	public MFATestPassEntry fetchByUserId(long userId, boolean useFinderCache) {
		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {userId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUserId, finderArgs, this);
		}

		if (result instanceof MFATestPassEntry) {
			MFATestPassEntry mfaTestPassEntry = (MFATestPassEntry)result;

			if (userId != mfaTestPassEntry.getUserId()) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_MFATESTPASSENTRY_WHERE);

			sb.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				List<MFATestPassEntry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUserId, finderArgs, list);
					}
				}
				else {
					MFATestPassEntry mfaTestPassEntry = list.get(0);

					result = mfaTestPassEntry;

					cacheResult(mfaTestPassEntry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (MFATestPassEntry)result;
		}
	}

	/**
	 * Removes the mfa test pass entry where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @return the mfa test pass entry that was removed
	 */
	@Override
	public MFATestPassEntry removeByUserId(long userId)
		throws NoSuchEntryException {

		MFATestPassEntry mfaTestPassEntry = findByUserId(userId);

		return remove(mfaTestPassEntry);
	}

	/**
	 * Returns the number of mfa test pass entries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching mfa test pass entries
	 */
	@Override
	public int countByUserId(long userId) {
		FinderPath finderPath = _finderPathCountByUserId;

		Object[] finderArgs = new Object[] {userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_MFATESTPASSENTRY_WHERE);

			sb.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 =
		"mfaTestPassEntry.userId = ?";

	public MFATestPassEntryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("password", "password_");

		setDBColumnNames(dbColumnNames);

		setModelClass(MFATestPassEntry.class);

		setModelImplClass(MFATestPassEntryImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the mfa test pass entry in the entity cache if it is enabled.
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 */
	@Override
	public void cacheResult(MFATestPassEntry mfaTestPassEntry) {
		entityCache.putResult(
			MFATestPassEntryImpl.class, mfaTestPassEntry.getPrimaryKey(),
			mfaTestPassEntry);

		finderCache.putResult(
			_finderPathFetchByUserId,
			new Object[] {mfaTestPassEntry.getUserId()}, mfaTestPassEntry);
	}

	/**
	 * Caches the mfa test pass entries in the entity cache if it is enabled.
	 *
	 * @param mfaTestPassEntries the mfa test pass entries
	 */
	@Override
	public void cacheResult(List<MFATestPassEntry> mfaTestPassEntries) {
		for (MFATestPassEntry mfaTestPassEntry : mfaTestPassEntries) {
			if (entityCache.getResult(
					MFATestPassEntryImpl.class,
					mfaTestPassEntry.getPrimaryKey()) == null) {

				cacheResult(mfaTestPassEntry);
			}
		}
	}

	/**
	 * Clears the cache for all mfa test pass entries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MFATestPassEntryImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the mfa test pass entry.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MFATestPassEntry mfaTestPassEntry) {
		entityCache.removeResult(MFATestPassEntryImpl.class, mfaTestPassEntry);
	}

	@Override
	public void clearCache(List<MFATestPassEntry> mfaTestPassEntries) {
		for (MFATestPassEntry mfaTestPassEntry : mfaTestPassEntries) {
			entityCache.removeResult(
				MFATestPassEntryImpl.class, mfaTestPassEntry);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(MFATestPassEntryImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		MFATestPassEntryModelImpl mfaTestPassEntryModelImpl) {

		Object[] args = new Object[] {mfaTestPassEntryModelImpl.getUserId()};

		finderCache.putResult(
			_finderPathCountByUserId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUserId, args, mfaTestPassEntryModelImpl, false);
	}

	/**
	 * Creates a new mfa test pass entry with the primary key. Does not add the mfa test pass entry to the database.
	 *
	 * @param mfaTestPassEntryId the primary key for the new mfa test pass entry
	 * @return the new mfa test pass entry
	 */
	@Override
	public MFATestPassEntry create(long mfaTestPassEntryId) {
		MFATestPassEntry mfaTestPassEntry = new MFATestPassEntryImpl();

		mfaTestPassEntry.setNew(true);
		mfaTestPassEntry.setPrimaryKey(mfaTestPassEntryId);

		return mfaTestPassEntry;
	}

	/**
	 * Removes the mfa test pass entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public MFATestPassEntry remove(long mfaTestPassEntryId)
		throws NoSuchEntryException {

		return remove((Serializable)mfaTestPassEntryId);
	}

	/**
	 * Removes the mfa test pass entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public MFATestPassEntry remove(Serializable primaryKey)
		throws NoSuchEntryException {

		Session session = null;

		try {
			session = openSession();

			MFATestPassEntry mfaTestPassEntry = (MFATestPassEntry)session.get(
				MFATestPassEntryImpl.class, primaryKey);

			if (mfaTestPassEntry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEntryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(mfaTestPassEntry);
		}
		catch (NoSuchEntryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MFATestPassEntry removeImpl(MFATestPassEntry mfaTestPassEntry) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(mfaTestPassEntry)) {
				mfaTestPassEntry = (MFATestPassEntry)session.get(
					MFATestPassEntryImpl.class,
					mfaTestPassEntry.getPrimaryKeyObj());
			}

			if (mfaTestPassEntry != null) {
				session.delete(mfaTestPassEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (mfaTestPassEntry != null) {
			clearCache(mfaTestPassEntry);
		}

		return mfaTestPassEntry;
	}

	@Override
	public MFATestPassEntry updateImpl(MFATestPassEntry mfaTestPassEntry) {
		boolean isNew = mfaTestPassEntry.isNew();

		if (!(mfaTestPassEntry instanceof MFATestPassEntryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(mfaTestPassEntry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					mfaTestPassEntry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in mfaTestPassEntry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom MFATestPassEntry implementation " +
					mfaTestPassEntry.getClass());
		}

		MFATestPassEntryModelImpl mfaTestPassEntryModelImpl =
			(MFATestPassEntryModelImpl)mfaTestPassEntry;

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(mfaTestPassEntry);
			}
			else {
				mfaTestPassEntry = (MFATestPassEntry)session.merge(
					mfaTestPassEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			MFATestPassEntryImpl.class, mfaTestPassEntryModelImpl, false, true);

		cacheUniqueFindersCache(mfaTestPassEntryModelImpl);

		if (isNew) {
			mfaTestPassEntry.setNew(false);
		}

		mfaTestPassEntry.resetOriginalValues();

		return mfaTestPassEntry;
	}

	/**
	 * Returns the mfa test pass entry with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the mfa test pass entry
	 * @return the mfa test pass entry
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public MFATestPassEntry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEntryException {

		MFATestPassEntry mfaTestPassEntry = fetchByPrimaryKey(primaryKey);

		if (mfaTestPassEntry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEntryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return mfaTestPassEntry;
	}

	/**
	 * Returns the mfa test pass entry with the primary key or throws a <code>NoSuchEntryException</code> if it could not be found.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public MFATestPassEntry findByPrimaryKey(long mfaTestPassEntryId)
		throws NoSuchEntryException {

		return findByPrimaryKey((Serializable)mfaTestPassEntryId);
	}

	/**
	 * Returns the mfa test pass entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry, or <code>null</code> if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public MFATestPassEntry fetchByPrimaryKey(long mfaTestPassEntryId) {
		return fetchByPrimaryKey((Serializable)mfaTestPassEntryId);
	}

	/**
	 * Returns all the mfa test pass entries.
	 *
	 * @return the mfa test pass entries
	 */
	@Override
	public List<MFATestPassEntry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @return the range of mfa test pass entries
	 */
	@Override
	public List<MFATestPassEntry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of mfa test pass entries
	 */
	@Override
	public List<MFATestPassEntry> findAll(
		int start, int end,
		OrderByComparator<MFATestPassEntry> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of mfa test pass entries
	 */
	@Override
	public List<MFATestPassEntry> findAll(
		int start, int end,
		OrderByComparator<MFATestPassEntry> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<MFATestPassEntry> list = null;

		if (useFinderCache) {
			list = (List<MFATestPassEntry>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_MFATESTPASSENTRY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_MFATESTPASSENTRY;

				sql = sql.concat(MFATestPassEntryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<MFATestPassEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the mfa test pass entries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (MFATestPassEntry mfaTestPassEntry : findAll()) {
			remove(mfaTestPassEntry);
		}
	}

	/**
	 * Returns the number of mfa test pass entries.
	 *
	 * @return the number of mfa test pass entries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_MFATESTPASSENTRY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "mfaTestPassEntryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_MFATESTPASSENTRY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MFATestPassEntryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the mfa test pass entry persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class,
			new MFATestPassEntryModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", MFATestPassEntry.class.getName()));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathFetchByUserId = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUserId",
			new String[] {Long.class.getName()}, new String[] {"userId"}, true);

		_finderPathCountByUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] {Long.class.getName()}, new String[] {"userId"},
			false);
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(MFATestPassEntryImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	@Override
	@Reference(
		target = MFATestPassPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = MFATestPassPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = MFATestPassPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_MFATESTPASSENTRY =
		"SELECT mfaTestPassEntry FROM MFATestPassEntry mfaTestPassEntry";

	private static final String _SQL_SELECT_MFATESTPASSENTRY_WHERE =
		"SELECT mfaTestPassEntry FROM MFATestPassEntry mfaTestPassEntry WHERE ";

	private static final String _SQL_COUNT_MFATESTPASSENTRY =
		"SELECT COUNT(mfaTestPassEntry) FROM MFATestPassEntry mfaTestPassEntry";

	private static final String _SQL_COUNT_MFATESTPASSENTRY_WHERE =
		"SELECT COUNT(mfaTestPassEntry) FROM MFATestPassEntry mfaTestPassEntry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "mfaTestPassEntry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No MFATestPassEntry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No MFATestPassEntry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		MFATestPassEntryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"password"});

	static {
		try {
			Class.forName(MFATestPassPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;
	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();

	private static class MFATestPassEntryModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return FINDER_ARGS_EMPTY;
				}

				return null;
			}

			MFATestPassEntryModelImpl mfaTestPassEntryModelImpl =
				(MFATestPassEntryModelImpl)baseModel;

			long columnBitmask = mfaTestPassEntryModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(
					mfaTestPassEntryModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						mfaTestPassEntryModelImpl.getColumnBitmask(columnName);
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(
					mfaTestPassEntryModelImpl, columnNames, original);
			}

			return null;
		}

		private Object[] _getValue(
			MFATestPassEntryModelImpl mfaTestPassEntryModelImpl,
			String[] columnNames, boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						mfaTestPassEntryModelImpl.getColumnOriginalValue(
							columnName);
				}
				else {
					arguments[i] = mfaTestPassEntryModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static Map<FinderPath, Long> _finderPathColumnBitmasksCache =
			new ConcurrentHashMap<>();

	}

}
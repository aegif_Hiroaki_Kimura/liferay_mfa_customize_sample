/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MFATestPassEntry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MFATestPassEntry
 * @generated
 */
public class MFATestPassEntryWrapper
	extends BaseModelWrapper<MFATestPassEntry>
	implements MFATestPassEntry, ModelWrapper<MFATestPassEntry> {

	public MFATestPassEntryWrapper(MFATestPassEntry mfaTestPassEntry) {
		super(mfaTestPassEntry);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("mfaTestPassEntryId", getMfaTestPassEntryId());
		attributes.put("userId", getUserId());
		attributes.put("password", getPassword());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long mfaTestPassEntryId = (Long)attributes.get("mfaTestPassEntryId");

		if (mfaTestPassEntryId != null) {
			setMfaTestPassEntryId(mfaTestPassEntryId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}
	}

	/**
	 * Returns the mfa test pass entry ID of this mfa test pass entry.
	 *
	 * @return the mfa test pass entry ID of this mfa test pass entry
	 */
	@Override
	public long getMfaTestPassEntryId() {
		return model.getMfaTestPassEntryId();
	}

	/**
	 * Returns the password of this mfa test pass entry.
	 *
	 * @return the password of this mfa test pass entry
	 */
	@Override
	public String getPassword() {
		return model.getPassword();
	}

	/**
	 * Returns the primary key of this mfa test pass entry.
	 *
	 * @return the primary key of this mfa test pass entry
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this mfa test pass entry.
	 *
	 * @return the user ID of this mfa test pass entry
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this mfa test pass entry.
	 *
	 * @return the user uuid of this mfa test pass entry
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the mfa test pass entry ID of this mfa test pass entry.
	 *
	 * @param mfaTestPassEntryId the mfa test pass entry ID of this mfa test pass entry
	 */
	@Override
	public void setMfaTestPassEntryId(long mfaTestPassEntryId) {
		model.setMfaTestPassEntryId(mfaTestPassEntryId);
	}

	/**
	 * Sets the password of this mfa test pass entry.
	 *
	 * @param password the password of this mfa test pass entry
	 */
	@Override
	public void setPassword(String password) {
		model.setPassword(password);
	}

	/**
	 * Sets the primary key of this mfa test pass entry.
	 *
	 * @param primaryKey the primary key of this mfa test pass entry
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this mfa test pass entry.
	 *
	 * @param userId the user ID of this mfa test pass entry
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this mfa test pass entry.
	 *
	 * @param userUuid the user uuid of this mfa test pass entry
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	@Override
	protected MFATestPassEntryWrapper wrap(MFATestPassEntry mfaTestPassEntry) {
		return new MFATestPassEntryWrapper(mfaTestPassEntry);
	}

}
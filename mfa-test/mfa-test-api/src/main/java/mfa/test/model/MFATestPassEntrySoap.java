/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class MFATestPassEntrySoap implements Serializable {

	public static MFATestPassEntrySoap toSoapModel(MFATestPassEntry model) {
		MFATestPassEntrySoap soapModel = new MFATestPassEntrySoap();

		soapModel.setMfaTestPassEntryId(model.getMfaTestPassEntryId());
		soapModel.setUserId(model.getUserId());
		soapModel.setPassword(model.getPassword());

		return soapModel;
	}

	public static MFATestPassEntrySoap[] toSoapModels(
		MFATestPassEntry[] models) {

		MFATestPassEntrySoap[] soapModels =
			new MFATestPassEntrySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MFATestPassEntrySoap[][] toSoapModels(
		MFATestPassEntry[][] models) {

		MFATestPassEntrySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new MFATestPassEntrySoap[models.length][models[0].length];
		}
		else {
			soapModels = new MFATestPassEntrySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MFATestPassEntrySoap[] toSoapModels(
		List<MFATestPassEntry> models) {

		List<MFATestPassEntrySoap> soapModels =
			new ArrayList<MFATestPassEntrySoap>(models.size());

		for (MFATestPassEntry model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MFATestPassEntrySoap[soapModels.size()]);
	}

	public MFATestPassEntrySoap() {
	}

	public long getPrimaryKey() {
		return _mfaTestPassEntryId;
	}

	public void setPrimaryKey(long pk) {
		setMfaTestPassEntryId(pk);
	}

	public long getMfaTestPassEntryId() {
		return _mfaTestPassEntryId;
	}

	public void setMfaTestPassEntryId(long mfaTestPassEntryId) {
		_mfaTestPassEntryId = mfaTestPassEntryId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}

	private long _mfaTestPassEntryId;
	private long _userId;
	private String _password;

}
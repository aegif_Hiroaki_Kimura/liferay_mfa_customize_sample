/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import mfa.test.model.MFATestPassEntry;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the mfa test pass entry service. This utility wraps <code>mfa.test.service.persistence.impl.MFATestPassEntryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MFATestPassEntryPersistence
 * @generated
 */
public class MFATestPassEntryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(MFATestPassEntry mfaTestPassEntry) {
		getPersistence().clearCache(mfaTestPassEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, MFATestPassEntry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<MFATestPassEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<MFATestPassEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<MFATestPassEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<MFATestPassEntry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static MFATestPassEntry update(MFATestPassEntry mfaTestPassEntry) {
		return getPersistence().update(mfaTestPassEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static MFATestPassEntry update(
		MFATestPassEntry mfaTestPassEntry, ServiceContext serviceContext) {

		return getPersistence().update(mfaTestPassEntry, serviceContext);
	}

	/**
	 * Returns the mfa test pass entry where userId = &#63; or throws a <code>NoSuchEntryException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @return the matching mfa test pass entry
	 * @throws NoSuchEntryException if a matching mfa test pass entry could not be found
	 */
	public static MFATestPassEntry findByUserId(long userId)
		throws mfa.test.exception.NoSuchEntryException {

		return getPersistence().findByUserId(userId);
	}

	/**
	 * Returns the mfa test pass entry where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @return the matching mfa test pass entry, or <code>null</code> if a matching mfa test pass entry could not be found
	 */
	public static MFATestPassEntry fetchByUserId(long userId) {
		return getPersistence().fetchByUserId(userId);
	}

	/**
	 * Returns the mfa test pass entry where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching mfa test pass entry, or <code>null</code> if a matching mfa test pass entry could not be found
	 */
	public static MFATestPassEntry fetchByUserId(
		long userId, boolean useFinderCache) {

		return getPersistence().fetchByUserId(userId, useFinderCache);
	}

	/**
	 * Removes the mfa test pass entry where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @return the mfa test pass entry that was removed
	 */
	public static MFATestPassEntry removeByUserId(long userId)
		throws mfa.test.exception.NoSuchEntryException {

		return getPersistence().removeByUserId(userId);
	}

	/**
	 * Returns the number of mfa test pass entries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching mfa test pass entries
	 */
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	 * Caches the mfa test pass entry in the entity cache if it is enabled.
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 */
	public static void cacheResult(MFATestPassEntry mfaTestPassEntry) {
		getPersistence().cacheResult(mfaTestPassEntry);
	}

	/**
	 * Caches the mfa test pass entries in the entity cache if it is enabled.
	 *
	 * @param mfaTestPassEntries the mfa test pass entries
	 */
	public static void cacheResult(List<MFATestPassEntry> mfaTestPassEntries) {
		getPersistence().cacheResult(mfaTestPassEntries);
	}

	/**
	 * Creates a new mfa test pass entry with the primary key. Does not add the mfa test pass entry to the database.
	 *
	 * @param mfaTestPassEntryId the primary key for the new mfa test pass entry
	 * @return the new mfa test pass entry
	 */
	public static MFATestPassEntry create(long mfaTestPassEntryId) {
		return getPersistence().create(mfaTestPassEntryId);
	}

	/**
	 * Removes the mfa test pass entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	public static MFATestPassEntry remove(long mfaTestPassEntryId)
		throws mfa.test.exception.NoSuchEntryException {

		return getPersistence().remove(mfaTestPassEntryId);
	}

	public static MFATestPassEntry updateImpl(
		MFATestPassEntry mfaTestPassEntry) {

		return getPersistence().updateImpl(mfaTestPassEntry);
	}

	/**
	 * Returns the mfa test pass entry with the primary key or throws a <code>NoSuchEntryException</code> if it could not be found.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	public static MFATestPassEntry findByPrimaryKey(long mfaTestPassEntryId)
		throws mfa.test.exception.NoSuchEntryException {

		return getPersistence().findByPrimaryKey(mfaTestPassEntryId);
	}

	/**
	 * Returns the mfa test pass entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry, or <code>null</code> if a mfa test pass entry with the primary key could not be found
	 */
	public static MFATestPassEntry fetchByPrimaryKey(long mfaTestPassEntryId) {
		return getPersistence().fetchByPrimaryKey(mfaTestPassEntryId);
	}

	/**
	 * Returns all the mfa test pass entries.
	 *
	 * @return the mfa test pass entries
	 */
	public static List<MFATestPassEntry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @return the range of mfa test pass entries
	 */
	public static List<MFATestPassEntry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of mfa test pass entries
	 */
	public static List<MFATestPassEntry> findAll(
		int start, int end,
		OrderByComparator<MFATestPassEntry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of mfa test pass entries
	 */
	public static List<MFATestPassEntry> findAll(
		int start, int end,
		OrderByComparator<MFATestPassEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the mfa test pass entries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of mfa test pass entries.
	 *
	 * @return the number of mfa test pass entries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MFATestPassEntryPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<MFATestPassEntryPersistence, MFATestPassEntryPersistence>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			MFATestPassEntryPersistence.class);

		ServiceTracker<MFATestPassEntryPersistence, MFATestPassEntryPersistence>
			serviceTracker =
				new ServiceTracker
					<MFATestPassEntryPersistence, MFATestPassEntryPersistence>(
						bundle.getBundleContext(),
						MFATestPassEntryPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}
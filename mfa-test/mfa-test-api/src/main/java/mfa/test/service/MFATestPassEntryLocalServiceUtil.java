/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.service;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for MFATestPassEntry. This utility wraps
 * <code>mfa.test.service.impl.MFATestPassEntryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MFATestPassEntryLocalService
 * @generated
 */
public class MFATestPassEntryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>mfa.test.service.impl.MFATestPassEntryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the mfa test pass entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 * @return the mfa test pass entry that was added
	 */
	public static mfa.test.model.MFATestPassEntry addMFATestPassEntry(
		mfa.test.model.MFATestPassEntry mfaTestPassEntry) {

		return getService().addMFATestPassEntry(mfaTestPassEntry);
	}

	public static mfa.test.model.MFATestPassEntry addTestPassEntry(
			long userId, String password)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().addTestPassEntry(userId, password);
	}

	/**
	 * Creates a new mfa test pass entry with the primary key. Does not add the mfa test pass entry to the database.
	 *
	 * @param mfaTestPassEntryId the primary key for the new mfa test pass entry
	 * @return the new mfa test pass entry
	 */
	public static mfa.test.model.MFATestPassEntry createMFATestPassEntry(
		long mfaTestPassEntryId) {

		return getService().createMFATestPassEntry(mfaTestPassEntryId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			createPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the mfa test pass entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 * @throws PortalException if a mfa test pass entry with the primary key could not be found
	 */
	public static mfa.test.model.MFATestPassEntry deleteMFATestPassEntry(
			long mfaTestPassEntryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteMFATestPassEntry(mfaTestPassEntryId);
	}

	/**
	 * Deletes the mfa test pass entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 */
	public static mfa.test.model.MFATestPassEntry deleteMFATestPassEntry(
		mfa.test.model.MFATestPassEntry mfaTestPassEntry) {

		return getService().deleteMFATestPassEntry(mfaTestPassEntry);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mfa.test.model.impl.MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mfa.test.model.impl.MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static mfa.test.model.MFATestPassEntry fetchMFATestPassEntry(
		long mfaTestPassEntryId) {

		return getService().fetchMFATestPassEntry(mfaTestPassEntryId);
	}

	public static mfa.test.model.MFATestPassEntry fetchMFATestPassEntryByUserId(
		long userId) {

		return getService().fetchMFATestPassEntryByUserId(userId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mfa.test.model.impl.MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @return the range of mfa test pass entries
	 */
	public static java.util.List<mfa.test.model.MFATestPassEntry>
		getMFATestPassEntries(int start, int end) {

		return getService().getMFATestPassEntries(start, end);
	}

	/**
	 * Returns the number of mfa test pass entries.
	 *
	 * @return the number of mfa test pass entries
	 */
	public static int getMFATestPassEntriesCount() {
		return getService().getMFATestPassEntriesCount();
	}

	/**
	 * Returns the mfa test pass entry with the primary key.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry
	 * @throws PortalException if a mfa test pass entry with the primary key could not be found
	 */
	public static mfa.test.model.MFATestPassEntry getMFATestPassEntry(
			long mfaTestPassEntryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getMFATestPassEntry(mfaTestPassEntryId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the mfa test pass entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 * @return the mfa test pass entry that was updated
	 */
	public static mfa.test.model.MFATestPassEntry updateMFATestPassEntry(
		mfa.test.model.MFATestPassEntry mfaTestPassEntry) {

		return getService().updateMFATestPassEntry(mfaTestPassEntry);
	}

	public static MFATestPassEntryLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<MFATestPassEntryLocalService, MFATestPassEntryLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			MFATestPassEntryLocalService.class);

		ServiceTracker
			<MFATestPassEntryLocalService, MFATestPassEntryLocalService>
				serviceTracker =
					new ServiceTracker
						<MFATestPassEntryLocalService,
						 MFATestPassEntryLocalService>(
							 bundle.getBundleContext(),
							 MFATestPassEntryLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}
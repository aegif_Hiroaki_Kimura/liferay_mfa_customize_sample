/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MFATestPassEntryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MFATestPassEntryLocalService
 * @generated
 */
public class MFATestPassEntryLocalServiceWrapper
	implements MFATestPassEntryLocalService,
			   ServiceWrapper<MFATestPassEntryLocalService> {

	public MFATestPassEntryLocalServiceWrapper(
		MFATestPassEntryLocalService mfaTestPassEntryLocalService) {

		_mfaTestPassEntryLocalService = mfaTestPassEntryLocalService;
	}

	/**
	 * Adds the mfa test pass entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 * @return the mfa test pass entry that was added
	 */
	@Override
	public mfa.test.model.MFATestPassEntry addMFATestPassEntry(
		mfa.test.model.MFATestPassEntry mfaTestPassEntry) {

		return _mfaTestPassEntryLocalService.addMFATestPassEntry(
			mfaTestPassEntry);
	}

	@Override
	public mfa.test.model.MFATestPassEntry addTestPassEntry(
			long userId, String password)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mfaTestPassEntryLocalService.addTestPassEntry(userId, password);
	}

	/**
	 * Creates a new mfa test pass entry with the primary key. Does not add the mfa test pass entry to the database.
	 *
	 * @param mfaTestPassEntryId the primary key for the new mfa test pass entry
	 * @return the new mfa test pass entry
	 */
	@Override
	public mfa.test.model.MFATestPassEntry createMFATestPassEntry(
		long mfaTestPassEntryId) {

		return _mfaTestPassEntryLocalService.createMFATestPassEntry(
			mfaTestPassEntryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mfaTestPassEntryLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the mfa test pass entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 * @throws PortalException if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public mfa.test.model.MFATestPassEntry deleteMFATestPassEntry(
			long mfaTestPassEntryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mfaTestPassEntryLocalService.deleteMFATestPassEntry(
			mfaTestPassEntryId);
	}

	/**
	 * Deletes the mfa test pass entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 */
	@Override
	public mfa.test.model.MFATestPassEntry deleteMFATestPassEntry(
		mfa.test.model.MFATestPassEntry mfaTestPassEntry) {

		return _mfaTestPassEntryLocalService.deleteMFATestPassEntry(
			mfaTestPassEntry);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mfaTestPassEntryLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _mfaTestPassEntryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _mfaTestPassEntryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mfa.test.model.impl.MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _mfaTestPassEntryLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mfa.test.model.impl.MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _mfaTestPassEntryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _mfaTestPassEntryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _mfaTestPassEntryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public mfa.test.model.MFATestPassEntry fetchMFATestPassEntry(
		long mfaTestPassEntryId) {

		return _mfaTestPassEntryLocalService.fetchMFATestPassEntry(
			mfaTestPassEntryId);
	}

	@Override
	public mfa.test.model.MFATestPassEntry fetchMFATestPassEntryByUserId(
		long userId) {

		return _mfaTestPassEntryLocalService.fetchMFATestPassEntryByUserId(
			userId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _mfaTestPassEntryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _mfaTestPassEntryLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mfa.test.model.impl.MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @return the range of mfa test pass entries
	 */
	@Override
	public java.util.List<mfa.test.model.MFATestPassEntry>
		getMFATestPassEntries(int start, int end) {

		return _mfaTestPassEntryLocalService.getMFATestPassEntries(start, end);
	}

	/**
	 * Returns the number of mfa test pass entries.
	 *
	 * @return the number of mfa test pass entries
	 */
	@Override
	public int getMFATestPassEntriesCount() {
		return _mfaTestPassEntryLocalService.getMFATestPassEntriesCount();
	}

	/**
	 * Returns the mfa test pass entry with the primary key.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry
	 * @throws PortalException if a mfa test pass entry with the primary key could not be found
	 */
	@Override
	public mfa.test.model.MFATestPassEntry getMFATestPassEntry(
			long mfaTestPassEntryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mfaTestPassEntryLocalService.getMFATestPassEntry(
			mfaTestPassEntryId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _mfaTestPassEntryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mfaTestPassEntryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the mfa test pass entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MFATestPassEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 * @return the mfa test pass entry that was updated
	 */
	@Override
	public mfa.test.model.MFATestPassEntry updateMFATestPassEntry(
		mfa.test.model.MFATestPassEntry mfaTestPassEntry) {

		return _mfaTestPassEntryLocalService.updateMFATestPassEntry(
			mfaTestPassEntry);
	}

	@Override
	public MFATestPassEntryLocalService getWrappedService() {
		return _mfaTestPassEntryLocalService;
	}

	@Override
	public void setWrappedService(
		MFATestPassEntryLocalService mfaTestPassEntryLocalService) {

		_mfaTestPassEntryLocalService = mfaTestPassEntryLocalService;
	}

	private MFATestPassEntryLocalService _mfaTestPassEntryLocalService;

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mfa.test.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mfa.test.exception.NoSuchEntryException;
import mfa.test.model.MFATestPassEntry;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the mfa test pass entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MFATestPassEntryUtil
 * @generated
 */
@ProviderType
public interface MFATestPassEntryPersistence
	extends BasePersistence<MFATestPassEntry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MFATestPassEntryUtil} to access the mfa test pass entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns the mfa test pass entry where userId = &#63; or throws a <code>NoSuchEntryException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @return the matching mfa test pass entry
	 * @throws NoSuchEntryException if a matching mfa test pass entry could not be found
	 */
	public MFATestPassEntry findByUserId(long userId)
		throws NoSuchEntryException;

	/**
	 * Returns the mfa test pass entry where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @return the matching mfa test pass entry, or <code>null</code> if a matching mfa test pass entry could not be found
	 */
	public MFATestPassEntry fetchByUserId(long userId);

	/**
	 * Returns the mfa test pass entry where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching mfa test pass entry, or <code>null</code> if a matching mfa test pass entry could not be found
	 */
	public MFATestPassEntry fetchByUserId(long userId, boolean useFinderCache);

	/**
	 * Removes the mfa test pass entry where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @return the mfa test pass entry that was removed
	 */
	public MFATestPassEntry removeByUserId(long userId)
		throws NoSuchEntryException;

	/**
	 * Returns the number of mfa test pass entries where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching mfa test pass entries
	 */
	public int countByUserId(long userId);

	/**
	 * Caches the mfa test pass entry in the entity cache if it is enabled.
	 *
	 * @param mfaTestPassEntry the mfa test pass entry
	 */
	public void cacheResult(MFATestPassEntry mfaTestPassEntry);

	/**
	 * Caches the mfa test pass entries in the entity cache if it is enabled.
	 *
	 * @param mfaTestPassEntries the mfa test pass entries
	 */
	public void cacheResult(
		java.util.List<MFATestPassEntry> mfaTestPassEntries);

	/**
	 * Creates a new mfa test pass entry with the primary key. Does not add the mfa test pass entry to the database.
	 *
	 * @param mfaTestPassEntryId the primary key for the new mfa test pass entry
	 * @return the new mfa test pass entry
	 */
	public MFATestPassEntry create(long mfaTestPassEntryId);

	/**
	 * Removes the mfa test pass entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry that was removed
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	public MFATestPassEntry remove(long mfaTestPassEntryId)
		throws NoSuchEntryException;

	public MFATestPassEntry updateImpl(MFATestPassEntry mfaTestPassEntry);

	/**
	 * Returns the mfa test pass entry with the primary key or throws a <code>NoSuchEntryException</code> if it could not be found.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry
	 * @throws NoSuchEntryException if a mfa test pass entry with the primary key could not be found
	 */
	public MFATestPassEntry findByPrimaryKey(long mfaTestPassEntryId)
		throws NoSuchEntryException;

	/**
	 * Returns the mfa test pass entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mfaTestPassEntryId the primary key of the mfa test pass entry
	 * @return the mfa test pass entry, or <code>null</code> if a mfa test pass entry with the primary key could not be found
	 */
	public MFATestPassEntry fetchByPrimaryKey(long mfaTestPassEntryId);

	/**
	 * Returns all the mfa test pass entries.
	 *
	 * @return the mfa test pass entries
	 */
	public java.util.List<MFATestPassEntry> findAll();

	/**
	 * Returns a range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @return the range of mfa test pass entries
	 */
	public java.util.List<MFATestPassEntry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of mfa test pass entries
	 */
	public java.util.List<MFATestPassEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MFATestPassEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the mfa test pass entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MFATestPassEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mfa test pass entries
	 * @param end the upper bound of the range of mfa test pass entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of mfa test pass entries
	 */
	public java.util.List<MFATestPassEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MFATestPassEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the mfa test pass entries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of mfa test pass entries.
	 *
	 * @return the number of mfa test pass entries
	 */
	public int countAll();

}
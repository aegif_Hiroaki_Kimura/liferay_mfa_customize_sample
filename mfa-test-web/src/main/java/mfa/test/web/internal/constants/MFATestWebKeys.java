package mfa.test.web.internal.constants;

/**
 * @author kimurahiroaki
 */
public class MFATestWebKeys {

	public static final String MFA_TEST_VALIDATED_USER_ID = "MFA_TEST_VALIDATED_USER_ID";
}
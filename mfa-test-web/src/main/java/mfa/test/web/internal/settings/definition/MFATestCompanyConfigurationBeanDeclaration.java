package mfa.test.web.internal.settings.definition;

import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;

import org.osgi.service.component.annotations.Component;

import mfa.test.web.internal.configuration.MFATestConfiguration;

@Component(
		property = "mfa.visibility.configuration.pid=mfa.test.web.internal.configuration.MFATestConfiguration",
		service = ConfigurationBeanDeclaration.class
)
public class MFATestCompanyConfigurationBeanDeclaration implements ConfigurationBeanDeclaration{
	
	@Override
	public Class<?> getConfigurationBeanClass(){
		return MFATestConfiguration.class;
	}

}

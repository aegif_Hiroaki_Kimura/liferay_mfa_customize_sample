package mfa.test.web.internal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "multi-factor-authentication",
		scope = ExtendedObjectClassDefinition.Scope.COMPANY
)
@Meta.OCD(
		id = "mfa.test.web.internal.configuration.MFATestConfiguration",
		localization = "content/Language", name = "MFA_Test_Configuration"
)
public interface MFATestConfiguration {
	@Meta.AD(deflt = "false", name = "enabled", required = false)
	public boolean enabled();

	@Meta.AD(deflt = "400", id = "service.ranking", name = "order", required = false)
	public int order();
}

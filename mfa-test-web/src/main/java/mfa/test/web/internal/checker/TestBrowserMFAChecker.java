package mfa.test.web.internal.checker;

import com.liferay.multi.factor.authentication.spi.checker.browser.BrowserMFAChecker;
import com.liferay.multi.factor.authentication.spi.checker.setup.SetupMFAChecker;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.HashMapDictionary;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

import java.util.Map;
import java.util.Objects;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import mfa.test.model.MFATestPassEntry;
import mfa.test.service.MFATestPassEntryLocalService;
import mfa.test.web.internal.configuration.MFATestConfiguration;
import mfa.test.web.internal.constants.MFATestWebKeys;


@Component(
		configurationPid = "mfa.test.web.internal.configuration.MFATestConfiguration.scoped",
		configurationPolicy = ConfigurationPolicy.REQUIRE, immediate=true,
		service = {}
)
public class TestBrowserMFAChecker implements BrowserMFAChecker, SetupMFAChecker {

	@Override
	public void includeBrowserVerification(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse, long userId) throws Exception {

		RequestDispatcher requestDispatcher = _servletContext.getRequestDispatcher("/mfa_test_checker/verify_browser.jsp");
		
		requestDispatcher.include(httpServletRequest, httpServletResponse);		
	}

	@Override
	public boolean isBrowserVerified(HttpServletRequest httpServletRequest, long userId) {

		HttpServletRequest originalHttpServletRequest = _portal.getOriginalServletRequest(httpServletRequest);
		
		HttpSession httpSession = originalHttpServletRequest.getSession(false);
		
		User user = _userLocalService.fetchUser(userId);
		
		if (user == null || httpSession == null) {
			return false;
		}
				
		Object mfaTestValidatedUserId = httpSession.getAttribute(MFATestWebKeys.MFA_TEST_VALIDATED_USER_ID);
		
		if(!Objects.equals(mfaTestValidatedUserId, userId)) {
			return false;
		}
		
		return true;
	}

	@Override
	public boolean verifyBrowserRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			long userId) throws Exception {

		String mfaTest = ParamUtil.getString(httpServletRequest, "mfaTest");

		if (Validator.isBlank(mfaTest)) {
			return false;
		}

		MFATestPassEntry mfaTestPassEntry = _mfaTestPassEntryLocalService.fetchMFATestPassEntryByUserId(userId);
		if (mfaTestPassEntry == null || !mfaTest.equals(mfaTestPassEntry.getPassword())) {
			return false;
		}
		HttpServletRequest originalHttpServletRequest = _portal.getOriginalServletRequest(httpServletRequest);

		HttpSession httpSession = originalHttpServletRequest.getSession();

		httpSession.setAttribute(MFATestWebKeys.MFA_TEST_VALIDATED_USER_ID, userId);

		return true;
	}

	@Activate
	protected void activate(BundleContext bundleContext, Map<String, Object> properties) {

		mfaTestConfiguration = 
				ConfigurableUtil.createConfigurable(MFATestConfiguration.class, properties);
		
		if (!mfaTestConfiguration.enabled()) {
			return;
		}

		_serviceRegistration = bundleContext.registerService(new String[] {
				BrowserMFAChecker.class.getName(), SetupMFAChecker.class.getName()
		}, this, new HashMapDictionary<>(properties));
	}
	
	@Deactivate
	protected void deactivate() {

		if (_serviceRegistration == null) {
			return;
		}
		
		_serviceRegistration.unregister();
	}
	
	
	
	@Override
	public void includeSetup(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			long userId) throws Exception {

		MFATestPassEntry mfaTestPassEntry = _mfaTestPassEntryLocalService.fetchMFATestPassEntryByUserId(userId);

		if (mfaTestPassEntry != null) {
			RequestDispatcher requestDispatcher = _servletContext.getRequestDispatcher("/mfa_test_checker/setup_completed.jsp");
			requestDispatcher.include(httpServletRequest, httpServletResponse);
		}else {
			RequestDispatcher requestDispatcher = _servletContext.getRequestDispatcher("/mfa_test_checker/setup.jsp");
			requestDispatcher.include(httpServletRequest, httpServletResponse);			
		}
		
	}

	@Override
	public void removeExistingSetup(long userId) {
		MFATestPassEntry mfaTestPassEntry = _mfaTestPassEntryLocalService.fetchMFATestPassEntryByUserId(userId);
		
		if (mfaTestPassEntry != null) {
			_mfaTestPassEntryLocalService.deleteMFATestPassEntry(mfaTestPassEntry);
		}
	}

	@Override
	public boolean setUp(HttpServletRequest httpServletRequest, long userId) {
		String mfaTest = ParamUtil.getString(httpServletRequest, "mfaTest");

		if (mfaTest!=null && !mfaTest.isEmpty()) {
			try {
				_mfaTestPassEntryLocalService.addTestPassEntry(userId, mfaTest);
				return true;
			}catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	


	@Override
	public boolean isAvailable(long userId) {

		MFATestPassEntry mfaTestPassEntry = _mfaTestPassEntryLocalService.fetchMFATestPassEntryByUserId(userId);
		if (mfaTestPassEntry != null) {
			return true;
		}
		return false;
	}

	private ServiceRegistration<?> _serviceRegistration;
	
	protected volatile MFATestConfiguration mfaTestConfiguration;
	
	@Reference
	private UserLocalService _userLocalService;
	
	@Reference
	private MFATestPassEntryLocalService _mfaTestPassEntryLocalService;

	@Reference
	private Portal _portal;
		
	@Reference(
			target = "(osgi.web.symbolicname=mfa.test.web)"		
	)
	private ServletContext _servletContext;
	
}
